<#
    .SYNOPSIS
        This script sends a notification email to user accounts to update their expiring password.
    .DESCRIPTION
        This script consist of three major parts:

        - Checks the password expiration date of all Active Directory accounts found within an Organizational Unit.

        - Calculates the days remaining until password expiration.

        - Sends emails to users N number of days prior to the evaluated password expiration time frame.
    .PARAMETER Credential
        Specifies the user account credentials to use to perform this task. The default credentials are the credentials of the currently logged on user
        unless the cmdlet is run from an Active Directory PowerShell provider drive. If the cmdlet is run from such a provider drive, the account
        associated with the drive is the default.

        To specify this parameter, you can type a user name, such as "User1" or "Domain01\User01" or you can specify a PSCredential object. If you specify a user name for this parameter, the cmdlet prompts for a password.

        You can also create a PSCredential object by using a script or by using the Get-Credential cmdlet. You can then set the Credential parameter to the PSCredential object The following example shows how to create credentials.

        $AdminCredentials = Get-Credential "Domain01\User01"

        The following shows how to set the Credential parameter to these credentials.

        -Credential $AdminCredentials
    .PARAMETER SearchBase
        Specifies an Active Directory path to search under.

        The following example shows how to set this parameter to search under an OU.

        -SearchBase "ou=MAPCOMUsers,dc=mapcom,dc=local"

        When the value of the SearchBase parameter is set to an empty string and you are connected to a GC port, all partitions will be searched. If the value of the SearchBase parameter is set to an empty string and you are not connected to a GC port, an error will be thrown.
    .PARAMETER Days
        Specifes the amount of days to evaluate password expirations. Default is 7.
    .PARAMETER SmtpServer
        Specifies the STMP mail server in which to relay email messages to users.
    .PARAMETER SendFrom
        Specifies the email account in which to send the email message.
    .INPUTS
        None
    .OUTPUTS
        None
    .EXAMPLE
        This example shows how you can run this script in PowerShell terminal with one line.
        This will query for all users under MAPCOMUsers and send an email to all accounts whose passwords expire within at most 14 days.

        .\Send-PasswordExpirationMessage.ps1 -Credential (Get-Credential) -SearchBase 'OU=MAPCOMUsers,DC=mapcom,DC=local' -Days 14 -SmtpServer 'prod1mail.mapcom.local' -SendFrom 'knguyen@mapcommunications.com'

    .EXAMPLE
        This example shows how you can "load" your parameters and then call the script.

        $Parameters = @{
            Credential      = (Get-Credential)
            SearchBase      = 'OU=MAPCOMUsers,DC=mapcom,DC=local'
            Days            = 14
            SmtpServer      = 'prod1mail.mapcom.local'
            SendFrom        = 'knguyen@mapcommunications.com'
        }
        .\Send-PasswordExpirationMessage.ps1 @Parameters

    .NOTES

#>
#Requires -Modules ActiveDirectory
[CmdletBinding()]

param
(
    <#
        .NOTES
            This section defines the -Flags or -Parameters that are to be fed into the script.

            The goal is to store the supplied values into variables, which in turn are used in the implementation of the script
    #>

    # -Credential parameter (optional)
    [Parameter(Mandatory = $false, HelpMessage = 'Specifies the user account credentials to use to perform this task.')]
    [System.Management.Automation.PSCredential] # define the type class of the variable
    $Credential, # name of the variable

    # -SearchBase parameter (mandatory)
    [Parameter(Mandatory = $true, HelpMessage = 'Specifies an Active Directory path to search under.')]
    [String]
    $SearchBase,

    # -Days parameter (optional, defaults to 7)
    [Parameter(Mandatory = $false, HelpMessage = 'Specifes the amount of days to evaluate password expirations.')]
    [Int]
    $Days = 7,

    # -SmtpServer parameter (mandatory)
    [Parameter(Mandatory = $true, HelpMessage = 'Specifies the STMP mail server in which to relay email messages to users.')]
    [String]
    $SmtpServer,

    # -SendFrom parameter (mandatory)
    [Parameter(Mandatory = $true, HelpMessage = 'Specifies the email account in which to send the email message.')]
    [String]
    $SendFrom
)

begin {
    <#
        .NOTES
            Begin block

            In this section is used to define and "pre-stage" functions and/or variables to be used in the "process" block of a script.

            This convention is optional; however, I prefer this style as it makes the order of execution written with clear intent. More explicit code is more maintainable.

            I consider the "begin" section the "ingredients".
    #>

    function Get-EmailSubjectLine($firstName, $numberOfDays) {
        <#
            .SYNOPSIS
                This function returns a subject line message for the email
            .PARAMETER firstName
                Specify the first name of the user who's password will expire.
            .PARAMETER numberOfDays
                Specify the number of days remaining until the user's password will expire.
            .INPUTS
                String
            .OUTPUTS
                String
            .EXAMPLE
                $subjectLine = Get-EmailSubjectLine('Natalie', '7')
        #>

        # formatting string using the format operator '-f'
        # I prefer this method of formatting strings bc it visibly breaks down where the data will be dynamically filled.
        return '{0}, your password will expire in less than {1} days.' -f $firstName, $numberOfDays

    }#end Get-EmailSubjectLine

    function Get-EmailMessageBody {
        <#
            .SYNOPSIS
                This function returns a message body for the email
        #>

        return 'Please navigate to our Self-Service Password Reset portal found at https://sspr.natalie.com to reset your password.'
    
    }#end Get-EmailMessageBody


    # Find all user accounts that are enabled and have expiring passwords
    
    # Here I like to specify my query flags in a [hashtable] and call it later
    # Viewing my parameters vertically helps with my sanity
    # I hate very long, wide lines
    $Parameters = @{
        Credential  = $Credential                                                       # $Credential is supplied by the script parameters
        SearchBase  = $SearchBase                                                       # $SearchBase is supplied by the script parameters
        Filter      = '(Enabled -eq "True") -and (PasswordNeverExpires -eq "False")'    # Only retrieve enabled accounts whose password's can expire
        Properties  = @(                                                                # Here we are defining the user attributes to capture and leverage them later
            'GivenName', 'Surname', 'EmailAddress', 'msDS-UserPasswordExpiryTimeComputed'
        )
        SearchScope = 'Subtree'                                                         # Search all child OUs too
        Verbose     = $true                                                             # Show us everything
    }
    $myUsers = Get-ADUsers @Parameters  # Here, I am using the Parameters defined above to be supplied to Get-ADUser
    # Fun Fact: you can view the parameters of this command by running in Powershell `Get-Help Get-ADUser`


    # Create warning dates for future password expirations
    # This variable will dynamically be evaluated based on the supplied parameter $Days
    $warningDate = (Get-Date).AddDays($Days).ToLongDateString()
}

process {
    <#
        .NOTES
            Process block

            Here is where I implement the core of the intended logic.

            This is where we cook up the sauce using all the ingredients.
    #>

    foreach ($user in $myUsers) {
        # Notice the implicit variable "$user" being declared; we will leverage this to "loop" through each user returned from our query earlier

        # Convert the attribute 'msDS-UserPasswordExpiryTimeComputed' to a more "sane" format
        $myPasswordExpiryDate = [datetime]::FromFileTime($user.'msDS-UserPasswordExpiryTimeComputed').ToLongDateString()

        # Now lets use that to evaluate a "true" condition
        if ($myPasswordExpiryDate -le $warningDate) {
            # Here, the user's password expirate date is less than the evaluation date
            # Send email to user
            $Parameters = @{                                                                # I really like vertical composition
                To         = $user.EmailAddress
                From       = $SendFrom                                                     # Value from the script parameter
                SmtpServer = $SmtpServer                                                   # Value from the script parameter
                Subject    = (Get-EmailSubjectLine($user.GivenName, $Days.ToString()))     # Calling function from our "ingredients"
                Body       = (Get-EmailMessageBody)                                        # Calling function from our "ingredients"
                Verbose    = $true                                                         # Bc verbosity is cool
            }
            Send-MailMessage @Parameters
        }#end if
    }#end foreach
}

end { Write-Verbose -Message 'Script completed.' }