<#
    .SYNOPSIS
        <Short description of script>
    .DESCRIPTION
        <Long description of script>
    .PARAMETER ParamA
        <Short description of parameter input>
    .PARAMETER ParamB
        <Short description of parameter input>
    .EXAMPLE
        <Example commandline use here, repeat for more than one example>
    .INPUTS
        <Inputs if any, otherwise state None>
    .OUTPUTS
        <Outputs if any, otherwise state None>
        Example:
        Log file stored in C:\Windows\Temp\<name>.log>
    .NOTES
        Do not run scripts without fully understanding the inputs/data it takes in, the processes it performs, and what the final outcome will be. Furthermore, it is irresponsible to run a script, and if necessary, take full accountability of reverting any actions, operations, or changes the script performs.

        Blindly running scripts on a local or remote machine is dangerous and should not be tolerated.

        Revision:       1
        Author:         <Name>
        Created:        <Creation Date>
        Updated:        <Last Modified Date>
        Changes:
            - Initial script development
#>
[CmdletBinding()]
param(
    <#
        Each parameter should be documented. To make easier to keep the comments
        synchronized with changes to parameters, the preferred location for
        parameter comments in within the 'param' block, directory above each parameter
    #>
)
begin {
    <#
        Use this block to initilize helper functions that can be reused within
        the script locally. Some usage examples could include functions to log
        or write out to standard output. An example is provided below:
    #>
    function Get-Message {
        <#
        .SYNOPSIS
            Helper function to show default message used in VERBOSE/DEBUG/WARNING
            and prepend with a timestamp
        .PARAMETER Message
            Message to be displayed
        .INPUTS
            System.String
        .EXAMPLE
            Get-DefaultMessage -Message "Issue connecting to host"
    #>
        [CmdletBinding()]
        param
        (
            [Parameter(Mandatory = $true)]
            [System.String]
            $Message
        )
        # Construct time for output
        $time = Get-Date -Format 'HH:mm:ss.fff'
        # Construct date for output
        $date = Get-Date -Format 'MM-dd-yyyy'
        # get hostname
        $hostName = "$env:ComputerName"
        $output = [String]::Format('{0} {1} {2} {3}', '[' + $time, $date + ']', '[' + $hostName + ']', '- ' + $Message)
        Write-Output $output
    }#Get-DefaultMessage

}#begin

process {
    <#
        Write the core of your script and logic in this section
    #>
}#process
end {
    Write-Verbose -Message (Get-Message -Message 'Script completed.')
}
